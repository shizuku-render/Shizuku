#pragma once

#include <vector>
#include <tacopie/tacopie>
#include "Serialize.hpp"

namespace Shizuku
{
  class TCPClient
  {
  private:

#ifndef SHIZUKU_OPTOUT_NETWORK
    tacopie::tcp_client client;
    std::vector<char> buffer;
#endif

  public:
#ifndef SHIZUKU_OPTOUT_NETWORK
    TCPClient() : client() {};
    ~TCPClient() {};
#else
    TCPClient() {};
    ~TCPClient() {};
#endif

    bool connect(const std::string host, int port)
    {
#ifndef SHIZUKU_OPTOUT_NETWORK
      try
      {
        client.connect(host, port);
        return true;
      }
      catch (const tacopie::tacopie_error& e)
      {
        return false;
      }
#else
      return false;
#endif
    }

    bool disconnect()
    {
#ifndef SHIZUKU_OPTOUT_NETWORK
      try
      {
        client.disconnect();
        return true;
      }
      catch (const tacopie::tacopie_error& e)
      {
        return false;
      }
#else
      return false;
#endif
    }

    bool isConnected()
    {
#ifndef SHIZUKU_OPTOUT_NETWORK
      return client.is_connected();
#else
      return false;
#endif
    }

    template<typename T>
    void write(const T& v)
    {
#ifndef SHIZUKU_OPTOUT_NETWORK
      for (const auto c : Serialize(v))
      {
        this->buffer.push_back(c);
      }
#endif
    }

    bool flush()
    {
#ifndef SHIZUKU_OPTOUT_NETWORK
      if (!isConnected()) {
        return false;
      }

      client.async_write({ this->buffer, nullptr });
      this->buffer.clear();
      return true;
#else
      return false;
#endif
    }
  };
}
