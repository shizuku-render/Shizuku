#pragma once

#include <climits>
#include "GPU.hpp"

namespace Shizuku
{
  class Random
  {
    unsigned int x;
    unsigned int y;
    unsigned int z;
    unsigned int w;

  public:

    // default seed
    Random() GPU : Random(123456789u, 362436069u, 521288629u, 88675123u){}
    // single seed
    explicit Random(unsigned int _s) GPU
    {
      x = _s = 1812433253U * (_s ^ (_s >> 30)) + 1;
      y = _s = 1812433253U * (_s ^ (_s >> 30)) + 2;
      z = _s = 1812433253U * (_s ^ (_s >> 30)) + 1;
      w = _s = 1812433253U * (_s ^ (_s >> 30)) + 4;
    }
    Random(unsigned int _x, unsigned int _y, unsigned int _z, unsigned int _w) GPU : x(_x), y(_y), z(_z), w(_w) {

    };

    Random(const Random& r) GPU : Random(r.x, r.y, r.z, r.w) {};

    unsigned int next() GPU
    {
      const auto t = x ^ (x << 11);
      x = y;
      y = z;
      z = w;
      return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
    }

    double next01() GPU
    {
      return static_cast<double>(next()) / static_cast<double>(UINT_MAX);
    }
  };
}
