#pragma once

#include "GPU.hpp"
#include "Vector.hpp"
#include "Math.hpp"

namespace Shizuku
{
  class Intersection
  {
  public:
    int objectId;
    double distance;
    Vec3<double> normal, pos;
    Intersection() GPU : objectId(-1), distance(INF), normal(), pos() {}
  };
}
