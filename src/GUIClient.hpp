#pragma once

#include "TCPClient.hpp"
#include "Color.hpp"

namespace Shizuku
{
  class GUIClient
  {
  private:
    TCPClient tcpClient;

  public:
    GUIClient(int port);
    void init(int w, int h);
    void setPixel(int x, int y, const Color<double> color);
  };
}