#include <tacopie/tacopie>
#include "GUIClient.hpp"

Shizuku::GUIClient::GUIClient(int port)
{
  tcpClient.connect("127.0.0.1", port);
}

void Shizuku::GUIClient::init(int w, int h)
{
  tcpClient.write(int32_t(2));
  tcpClient.write(static_cast<int32_t>(w));
  tcpClient.write(static_cast<int32_t>(h));
}

void Shizuku::GUIClient::setPixel(int x, int y, const Color<double> color)
{
  tcpClient.write(int32_t(1));
  tcpClient.write(static_cast<int32_t>(x));
  tcpClient.write(static_cast<int32_t>(y));
  tcpClient.write(color.r);
  tcpClient.write(color.g);
  tcpClient.write(color.b);
  tcpClient.write(color.a);
  tcpClient.flush();
}