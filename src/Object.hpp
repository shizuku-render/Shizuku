#pragma once

#include "GPU.hpp"
#include "Material.hpp"
#include "Sphere.hpp"
#include "Intersection.hpp"

namespace Shizuku
{
  enum class ShapeType { Sphere };

  class Object
  {
  public:
    int id;

    // shape のうち shapeType で指定された一つを使う。
    // 本来なら、Shape を継承した Sphere や Polygon を作りたいが、C++ AMP では仮想関数が使えないので、やむなくここで吸収している。
    Sphere shapeSphere;
    ShapeType shapeType;

    // Polygon shapePolygon;

    Material material;

    Object(int _id, const Sphere& _shape, const Material& _material) GPU
      : id(_id)
      , shapeType(ShapeType::Sphere)
      , shapeSphere(_shape)
      , material(_material) {}

    bool intersect(const Ray &ray, Intersection * intersection) const GPU
    {
      bool ret;
      switch (shapeType)
      {
        case ShapeType::Sphere: ret = shapeSphere.intersect(ray, intersection); break;
        default:
          return false;
      }

      if (ret)
      {
        intersection->objectId = id;
      }
      return ret;
    }
  };
}
