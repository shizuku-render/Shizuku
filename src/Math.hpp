#pragma once

#include "GPU.hpp"

#define PI (3.14159265358979323846)
#define INF (1e128)
#define EPS (1e-6)

namespace Shizuku
{
  template<typename T=double>
  T Sqrt(T v) GPU
  {
    return precise_math::sqrt(v);
  }

  template<typename T = double>
  T Sin(T v) GPU
  {
    return precise_math::sin(v);
  }

  template<typename T = double>
  T Cos(T v) GPU
  {
    return precise_math::cos(v);
  }

  template<typename T1, typename T2>
  T1 Pow(T1 lhs, T2 rhs) GPU
  {
    return precise_math::pow(lhs, rhs);
  }

}
