#pragma once

#include <cstdint>
#include <vector>

namespace Shizuku
{
  template <typename T>
  std::vector<char> Serialize(const T& v)
  {
    return v.serialize();
  }

  template <>
  std::vector<char> Serialize(const std::int32_t& v);
  template <>
  std::vector<char> Serialize(const double& v);
}
