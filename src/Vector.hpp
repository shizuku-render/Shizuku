#pragma once

#include "GPU.hpp"
#include "Math.hpp"

namespace Shizuku {
  template <typename T>
  class Vec2
  {
  public:
    T x, y;
    Vec2() GPU {};
    Vec2(const Vec2& v) GPU : x(v.x), y(v.y) {};
    Vec2(T _x, T _y) GPU : x(_x), y(_y) {}

    static Vec2 Zero() GPU
    {
      return Vec2(0, 0);
    }

    static Vec2 Up() GPU
    {
      return Vec2(0, 1);
    }

    static Vec2 Down() GPU
    {
      return Vec2(0, -1);
    }

    static Vec2 Right() GPU
    {
      return Vec2(0, 1);
    }

    static Vec2 Left() GPU
    {
      return Vec2(0, -1);
    }

    Vec2 operator+() const GPU
    {
      return *this;
    }

    Vec2 operator-() const GPU
    {
      return { -x, -y };
    }

    Vec2 operator+(const Vec2& rhs) const GPU
    {
      return { x + rhs.x, y + rhs.y };
    }

    Vec2 operator-(const Vec2& rhs) const GPU
    {
      return { x - rhs.x, y - rhs.y };
    }

    Vec2 operator+(const T rhs) const GPU
    {
      return { x + rhs, y + rhs };
    }

    Vec2 operator-(const T rhs) const GPU
    {
      return { x - rhs, y - rhs };
    }

    Vec2 operator*(const T rhs) const GPU
    {
      return { x * rhs, y * rhs };
    }

    Vec2 operator/(const T rhs) const GPU
    {
      return { x / rhs, y / rhs };
    }

    Vec2 operator+=(const Vec2& rhs) GPU
    {
      x += rhs.x;
      y += rhs.y;
      return *this;
    }

    Vec2 operator-=(const Vec2& rhs) GPU
    {
      x -= rhs.x;
      y -= rhs.y;
      return *this;
    }

    Vec2 operator*=(const T rhs) GPU
    {
      x *= rhs;
      y *= rhs;
      return *this;
    }

    Vec2 operator/=(const T rhs) GPU
    {
      x /= rhs;
      y /= rhs;
      return *this;
    }

    bool operator ==(const Vec2& rhs) GPU
    {
      return x == rhs.x && y == rhs.y;
    }

    bool operator !=(const Vec2& rhs) GPU
    {
      return !(*this == rhs);
    }

    T dot(const Vec2& rhs) const GPU
    {
      return x * rhs.x, y * rhs.y;
    }

    T cross(const Vec2 rhs) const GPU
    {
      return x * rhs.y - y * rhs.x;
    }

    T static dot(const Vec2& lhs, const Vec2& rhs) GPU
    {
      return lhs.dot(rhs);
    }

    T static cross(const Vec2& lhs, const Vec2 rhs) GPU
    {
      return lhs.cross(rhs);
    }

    T lengthSquared() const GPU
    {
      return dot(*this);
    }

    T length() const GPU
    {
      return fast_math::sqrt(lengthSquared());
    }

    T distanceFrom(const Vec2& v) const GPU
    {
      return (*this - v).length();
    }

    Vec2 normalized() const GPU
    {
      return *this / length();
    }
  };

  template <typename T>
  Vec2<T> operator+(const T lhs, Vec2<T> rhs) GPU
  {
    return rhs + lhs;
  }

  template <typename T>
  Vec2<T> operator*(const T lhs, Vec2<T> rhs) GPU
  {
    return rhs * lhs;
  }

  template <typename T>
  class Vec3
  {
  public:
    T x, y, z;
    Vec3() GPU {};
    Vec3(const Vec3& v) GPU : x(v.x), y(v.y), z(v.z) {};
    Vec3(T _x, T _y, T _z) GPU : x(_x), y(_y), z(_z) {}
    Vec3(const Vec2<T>& xy, T _z) GPU : x(xy.x), y(xy.y), z(_z) {}
    Vec3(T _x, const Vec2<T>& yz) GPU : x(_x), y(yz.y), z(yz.z) {}

    static Vec3 Zero() GPU
    {
      return Vec3(0, 0, 0);
    }

    static Vec3 Up() GPU
    {
      return Vec3(0, 1, 0);
    }

    static Vec3 Down() GPU
    {
      return Vec3(0, -1, 0);
    }

    static Vec3 Right() GPU
    {
      return Vec3(0, 1, 0);
    }

    static Vec3 Left() GPU
    {
      return Vec3(0, -1, 0);
    }

    static Vec3 Foward() GPU
    {
      return Vec3(0, 0, -1);
    }

    static Vec3 Back() GPU
    {
      return Vec3(0, 0, 1);
    }

    Vec2<T> xy() const GPU
    {
      return { x, y };
    }

    Vec2<T> xz() const GPU
    {
      return { x, z };
    }

    Vec2<T> yx() const GPU
    {
      return { y, x };
    }

    Vec2<T> yz() const GPU
    {
      return { y, z };
    }

    Vec2<T> zx() const GPU
    {
      return { z, x };
    }

    Vec2<T> zy() const GPU
    {
      return { z, y };
    }

    Vec3 operator+() const GPU
    {
      return *this;
    }

    Vec3 operator-() const GPU
    {
      return { -x, -y, -z };
    }

    Vec3 operator+(const Vec3& rhs) const GPU
    {
      return { x + rhs.x, y + rhs.y, z + rhs.z };
    }

    Vec3 operator-(const Vec3& rhs) const GPU
    {
      return { x - rhs.x, y - rhs.y, z - rhs.z };
    }

    Vec3 operator+(const T rhs) const GPU
    {
      return { x + rhs, y + rhs, z + rhs };
    }

    Vec3 operator-(const T rhs) const GPU
    {
      return { x - rhs, y - rhs, z - rhs };
    }

    Vec3 operator*(const T rhs) const GPU
    {
      return { x * rhs, y * rhs, z * rhs };
    }

    Vec3 operator/(const T rhs) const GPU
    {
      return { x / rhs, y / rhs, z / rhs };
    }

    Vec3 operator+=(const Vec3& rhs) GPU
    {
      x += rhs.x;
      y += rhs.y;
      z += rhs.z;
      return *this;
    }

    Vec3 operator-=(const Vec3& rhs) GPU
    {
      x -= rhs.x;
      y -= rhs.y;
      z -= rhs.z;
      return *this;
    }

    Vec3 operator*=(const T rhs) GPU
    {
      x *= rhs;
      y *= rhs;
      z *= rhs;
      return *this;
    }

    Vec3 operator/=(const T rhs) GPU
    {
      x /= rhs;
      y /= rhs;
      z /= rhs;
      return *this;
    }

    bool operator ==(const Vec3& rhs) GPU
    {
      return x == rhs.x && y == rhs.y && z == rhs.z;
    }

    bool operator !=(const Vec3& rhs) GPU
    {
      return !(*this == rhs);
    }

    T dot(const Vec3& rhs) const GPU
    {
      return x * rhs.x + y * rhs.y + z * rhs.z;
    }

    Vec3 cross(const Vec3 rhs) const GPU
    {
      return{ y * rhs.z - z * rhs.y, z * rhs.x - x * rhs.z, x * rhs.y - y * rhs.x };
    }

    T static dot(const Vec3& lhs, const Vec3& rhs) GPU
    {
      return lhs.dot(rhs);
    }

    Vec3 static cross(const Vec3& lhs, const Vec3 rhs) GPU
    {
      return lhs.cross(rhs);
    }

    T lengthSquared() const GPU
    {
      return dot(*this);
    }

    T length() const GPU
    {
      return Sqrt(lengthSquared());
    }

    T distanceFrom(const Vec3& v) const GPU
    {
      return (*this - v).length();
    }

    Vec3 normalized() const GPU
    {
      return *this / length();
    }
  };

  template <typename T>
  Vec3<T> operator+(const T lhs, Vec3<T> rhs) GPU
  {
    return rhs + lhs;
  }

  template <typename T>
  Vec3<T> operator*(const T lhs, Vec3<T> rhs) GPU
  {
    return rhs * lhs;
  }

  template<typename T>
  using Size = Vec2<T>;
  template<typename T>
  using Point = Vec2<T>;
}
