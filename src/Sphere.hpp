#pragma once
#include "GPU.hpp"
#include "Vector.hpp"
#include "Ray.hpp"
#include "Intersection.hpp"

namespace Shizuku
{
  class Sphere
  {
  public:
    Vec3<double> center;
    double radius;

    Sphere(const Vec3<double>& _center, double _radius) GPU : center(_center), radius(_radius) {}

    bool intersect(const Ray& ray, Intersection * intersection) const GPU
    {
      const auto po = center - ray.org;
      const auto b = Vec3<double>::dot(po, ray.dir);
      const auto d4 = b * b - Vec3<double>::dot(po, po) + radius * radius;

      if (d4 < 0)
      {
        return false;
      }

      const auto sqrtD4 = Sqrt(d4);
      const auto t1 = b - sqrtD4;
      const auto t2 = b + sqrtD4;

      if (t1 < EPS && t2 < EPS)
      {
        return false;
      }

      intersection->distance = t1 > EPS ? t1 : t2;
      intersection->pos = ray.org + intersection->distance * ray.dir;
      intersection->normal = (intersection->pos - center) / radius;
      return true;
    }
  };
}
