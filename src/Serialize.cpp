#include "Serialize.hpp"

template <>
std::vector<char> Shizuku::Serialize(const std::int32_t& v)
{
  return {
    static_cast<char>(v),
    static_cast<char>(v << 8),
    static_cast<char>(v << 16),
    static_cast<char>(v << 24),
  };
}

template <>
std::vector<char> Shizuku::Serialize(const double& v)
{
  double d = v;
  auto p = reinterpret_cast<char*>(&d);
  return std::vector<char>(p, p + sizeof(double));

}