#pragma once

#include "GPU.hpp"

namespace Shizuku
{
  template <typename T>
  class Color
  {
  public:
    T r, g, b, a;

    Color() GPU : Color(static_cast<T>(0), static_cast<T>(0), static_cast<T>(0)) {}
    Color(const T& _r, const T& _g, const T& _b) GPU : Color(_r, _g, _b, static_cast<T>(1)) {}
    Color(const T& _r, const T& _g, const T& _b, const T& _a) GPU : r(_r), g(_g), b(_b), a(_a) {}

    Color operator+() const GPU
    {
      return *this;
    }

    Color operator+(const Color& rhs) const GPU
    {
      return { r + rhs.r, g + rhs.g, b + rhs.b, a + rhs.a };
    }

    Color operator-(const Color& rhs) const GPU
    {
      return { r - rhs.r, g - rhs.g, b - rhs.b, a - rhs.a };
    }

    Color operator*(const T rhs) const GPU
    {
      return { r * rhs, g * rhs, b * rhs, a * rhs };
    }

    Color operator/(const T rhs) const GPU
    {
      return { r / rhs, g / rhs, b / rhs, a / rhs };
    }

    Color operator+=(const Color& rhs) GPU
    {
      r += rhs.r;
      g += rhs.g;
      b += rhs.b;
      a += rhs.a;
      return *this;
    }

    Color operator-=(const Color& rhs) GPU
    {
      r -= rhs.r;
      g -= rhs.g;
      b -= rhs.b;
      a -= rhs.a;
      return *this;
    }

    Color operator*=(const T rhs) GPU
    {
      r *= rhs;
      g *= rhs;
      b *= rhs;
      a *= rhs;
      return *this;
    }

    Color operator/=(const T rhs) GPU
    {
      r /= rhs;
      g /= rhs;
      b /= rhs;
      a /= rhs;
      return *this;
    }

    bool operator ==(const Color& rhs) GPU
    {
      return r == rhs.r && g == rhs.g && b == rhs.b && a == rhs.a;
    }

    bool operator !=(const Color& rhs) GPU
    {
      return !(*this == rhs);
    }
  };
}
