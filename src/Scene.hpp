#pragma once

#include <initializer_list>
#include "GPU.hpp"
#include "Object.hpp"
#include "Sphere.hpp"
#include "Intersection.hpp"
#include "Ray.hpp"
#include "Math.hpp"

namespace Shizuku
{
  // Scene から参照する、オブジェクトが入った場所
  class SceneSource
  {
  public:
    array<Object> objects;
    array_view<Object> objectsView;
    SceneSource(std::initializer_list<Object> _objects)
      : objects(_objects.size(), _objects.begin(), _objects.end())
      , objectsView(objects) {}
  };

  class Scene
  {

  public:
    array_view<Object> objects;
    Scene(const SceneSource& src) : objects(src.objectsView) {}

    bool intersect(const Ray& ray, Intersection * intersection) const GPU
    {
      for (auto i = 0; i < objects.get_extent().size(); i++)
      {
        const auto & obj = objects[i];
        Intersection objIntersection;
        if (obj.intersect(ray, &objIntersection))
        {
          if (objIntersection.distance < intersection->distance)
          {
            *intersection = objIntersection;
          }
        }
      }
      return intersection->objectId != -1;
    }
  };
}
