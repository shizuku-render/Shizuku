#pragma once

#ifdef __HCC__
  #define SHIZUKU_GPU_HC 1
#else
  #define SHIZUKU_GPU_AMP 1
#endif

#ifdef SHIZUKU_GPU_AMP
  #include <amp.h>
  #include <amp_math.h>
  #define GPU __GPU

  using namespace concurrency;
#endif

#ifdef SHIZUKU_GPU_HC
  #include <hc.hpp>
  #include <hc_math.hpp>

  #define GPU __CPU__ __HC__
  using namespace hc;
#endif
