#pragma once

#include "GPU.hpp"
#include "Vector.hpp"
#include "Random.hpp"
#include "Math.hpp"

namespace Shizuku
{
  class Camera
  {
  public:
    Size<int> imageSize;
    int samplesPerPixel;

    Vec3<double> imagesensorCenter, imagesensorDir, imagesensorUp;
    Size<double> imagesensorSize;
    Vec3<double> imagesensorU, imagesensorV;
    Size<double> pixelSize;

    double imagesensorToLensDistance, focalLength, lensRadius, sensitivity;

    Vec3<double> objectplaneCenter, objectplaneU, objectplaneV;

    Vec3<double> lensCenter, lensU, lensV, normalOnLens;

    Camera() = delete;
    Camera(
      const Size<int>& _imageSize,
      const int _samplesPerPixel,
      const Vec3<double>& _imagesensorCenter,
      const Vec3<double>& _imagesensorDir,
      const Vec3<double>& _imagesensorUp,
      const double _imagesensorSize,
      const double _imagesensorToLensDistance,
      const double _focalLength,
      const double _lensRadius,
      const double _sensitivityBase)
      : imageSize(_imageSize)
      , imagesensorCenter(_imagesensorCenter)
      , imagesensorDir(_imagesensorDir.normalized())
      , imagesensorUp(_imagesensorUp)
      , imagesensorSize(_imagesensorSize * imageSize.x / imageSize.y, _imagesensorSize)
      , imagesensorU((Vec3<double>::cross(imagesensorDir, imagesensorUp) * imagesensorSize.x).normalized())
      , imagesensorV((Vec3<double>::cross(imagesensorU, imagesensorDir) * imagesensorSize.y).normalized())
      , pixelSize(imagesensorSize.x / imageSize.x, imagesensorSize.y / imageSize.y)
      , imagesensorToLensDistance(_imagesensorToLensDistance)
      , focalLength(_focalLength)
      , lensRadius(_lensRadius)
      , sensitivity(_sensitivityBase / (pixelSize.x * pixelSize.y))
      , objectplaneCenter(imagesensorCenter + (focalLength + imagesensorToLensDistance * imagesensorDir))
      , objectplaneU(imagesensorU)
      , objectplaneV(imagesensorV)
      , lensCenter(imagesensorCenter + (focalLength + imagesensorToLensDistance) * imagesensorDir)
      , lensU(imagesensorU)
      , lensV(imagesensorV)
      , normalOnLens(imagesensorDir.normalized()) {}

    void samplePoints(
      Vec3<double> * posOnSensor, Vec3<double> * posOnObjPlane, Vec3<double> * posOnLens,
      double * PImage, double * PLens,
      const Size<int> pos, Random * rand) const GPU
    {
      const Vec2<double> uvOnPixel = { rand->next01(), rand->next01() };
      const Vec2<double> uvOnSensor = {
        (pos.x + uvOnPixel.x) / (imageSize.x - 0.5),
        (pos.y + uvOnPixel.y) / (imageSize.y - 0.5),
      };
      *posOnSensor = imagesensorCenter + imagesensorU * uvOnSensor.x + imagesensorV * uvOnSensor.y;

      const double ratio = focalLength / imagesensorToLensDistance;
      const Vec2<double> uvOnObjPlane = uvOnSensor * -ratio;
      *posOnObjPlane = objectplaneCenter + objectplaneU * uvOnObjPlane.x + objectplaneV * uvOnObjPlane.y;

      const auto r0 = Sqrt(rand->next01());
      const auto r1 = rand->next01() * 2.0 * PI;
      const Vec2<double> uvOnLens = { r0 * Cos(r1), r0 * Sin(r1) };
      *posOnLens = lensCenter + uvOnLens.x * lensU + uvOnLens.y * lensV;

      *PImage = 1.0 / (pixelSize.x * pixelSize.y);
      *PLens = 1.0 / (PI * lensRadius * lensRadius);
    }
  };
}
