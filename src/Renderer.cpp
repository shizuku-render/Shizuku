#ifdef _WIN32
// windows.h より先に include する必要があり、amp.h が windows.h を include しているので、amp.h より前に置く必要がある
#include <WinSock2.h>
#endif

#include <iostream>
#include "GPU.hpp"
#include "Renderer.hpp"
#include "Image.hpp"
#include "Camera.hpp"
#include "Color.hpp"
#include "Random.hpp"
#include "Ray.hpp"
#include "Intersection.hpp"
#include "Object.hpp"
#include "Sphere.hpp"
#include "Scene.hpp"

namespace Shizuku
{
  Color<double> calcRadiance(const Ray& ray, const Scene & scene, Random * rand) GPU
  {
    Intersection intersection;
    if (scene.intersect(ray, &intersection)) {
      return { 1, 1, 1 };
    }
    return  { 0, 0, 0 };
  }

  void Renderer::Render(std::string fname) const
  {
    const int w = camera.imageSize.x;
    const int h = camera.imageSize.y;
    array<Color<double>, 2> data(h, w);
    array_view<Color<double>, 2> dataView(data);

    const auto& cam = camera;
    const Scene& scene(sceneSource);

    const auto kernel = [w, h, dataView, cam, scene](index<2> idx) GPU
    {
      const auto x = idx[1];
      const auto y = idx[0];
      Random rand(x + y * w + 1);

      Vec3<double> posOnSensor, posOnObjPlane, posOnLens;
      double PImage, PLens;
      cam.samplePoints(&posOnSensor, &posOnObjPlane, &posOnLens, &PImage, &PLens, { x, y }, &rand);

      Ray ray = { posOnLens, (posOnObjPlane - posOnLens).normalized() };
      const auto L = calcRadiance(ray, scene, &rand);
      const auto x0_xI = posOnSensor - posOnLens;
      const double coefficient = Pow(Vec3<double>::dot((-cam.imagesensorDir), x0_xI.normalized()), 2.0) / x0_xI.lengthSquared();

      dataView[idx] = L * cam.sensitivity * coefficient / (PImage * PLens);
    };
    gui->init(camera.imageSize.x, camera.imageSize.y);
    parallel_for_each(dataView.get_extent(), kernel);
    Random rand1(1), rand2(2);
    Image<int> img(camera.imageSize);
    for (auto y = 0; y < camera.imageSize.y; y++)
    {
      for (auto x = 0; x < camera.imageSize.x; x++)
      {
        const auto c = dataView(y)(x);
        img.setPixel(x, y, Color<int>(
          static_cast<int>(c.r * 255),
          static_cast<int>(c.g * 255),
          static_cast<int>(c.b * 255),
          static_cast<int>(c.a * 255)));
        gui->setPixel(x, y, Color<double>(x / (double)camera.imageSize.x, y / (double)camera.imageSize.y, 0));
        std::cout << "("
          << img.pixel(x, y).r << ", "
          << img.pixel(x, y).g << ", "
          << img.pixel(x, y).b << ")";
      }
      std::cout << std::endl;
    }

    img.saveAsPpm(fname);
  }
}
