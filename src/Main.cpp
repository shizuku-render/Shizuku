#ifdef _WIN32
#include <WinSock2.h>
#endif

#include <memory>
#include <iostream>
#include "GUIClient.hpp"
#include "Renderer.hpp"
#include "Camera.hpp"

using namespace Shizuku;
int main()
{
#ifdef _WIN32
  //! Windows netword DLL init
  WORD version = MAKEWORD(2, 2);
  WSADATA data;

  if (WSAStartup(version, &data) != 0) {
    std::cerr << "WSAStartup() failure" << std::endl;
    return -1;
  }
#endif

  const auto size = Size<int>(30, 20);
  const auto r = Renderer(
    Camera(size, 10, { 50, 40.8, 220 },
      Vec3<double>::Foward(),
      Vec3<double>::Up(),
      30, 30, 140, 5, 30),
    {
      Object(0, Sphere(Vec3<double>(50, 20, 50), 20), Material(Color<double>(1, 0, 0), Color<double>(0, 0, 0))),
    },
    std::make_shared<GUIClient>(50000)
  );
  r.Render("./out.ppm");

#ifdef _WIN32
  WSACleanup();
#endif
  return 0;
}
