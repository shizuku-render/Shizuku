#pragma once

#include <array>
#include <fstream>
#include <string>
#include "Vector.hpp"
#include "Color.hpp"

namespace Shizuku
{
  template<typename T>
  class Image
  {
    int w, h;
    std::vector<Color<T>> data;
  public:
    explicit Image(const Vec2<int>& size) : Image(size.x, size.y) {}
    Image(int _w, int _h) : w(_w), h(_h), data(w * h) {}
    ~Image() = default;

    Color<T> pixel(int x, int y) const
    {
      return data[x * y];
    };

    Color<T> setPixel(int x, int y, Color<T> c)
    {
      return data[x * y] = c;
    };

    const std::vector<Color<T>>& pixels() const
    {
      return data;
    }

    void saveAsPpm(std::string filename) const
    {
      std::ofstream fs(filename, std::ios::out);

      fs << "P3" << std::endl << w << " " << h << std::endl << "255" << std::endl;
      for (auto y = 0; y < h; y++)
      {
        for (auto x = 0; x < w; x++)
        {
          const auto p = pixel(x, y);
          fs << p.r << " " << p.g << " " << p.b;
          if (x < w - 1)
          {
            fs << " ";
          }
        }
        fs << "\n";
      }
    }
  };
}
