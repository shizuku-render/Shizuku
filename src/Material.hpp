#pragma once

#include "GPU.hpp"
#include "Color.hpp"

namespace Shizuku
{
  class Material
  {
  public:
    Color<double> diffuse, emission;

    Material(
      const Color<double>& _diffuse,
      const Color<double>& _emission) GPU
      : diffuse(_diffuse)
      , emission(_emission)
    {}
  };
}
