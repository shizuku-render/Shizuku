#pragma once

#include <string>
#include <memory>
#include "Camera.hpp"
#include "Scene.hpp"
#include "GUIClient.hpp"
namespace Shizuku
{
  class Renderer
  {
  private:
    Camera camera;
    SceneSource sceneSource;
    std::shared_ptr<GUIClient> gui;
  public:
    Renderer() = delete;
    Renderer(const Camera& cam, const SceneSource& _scene, const std::shared_ptr<GUIClient>& _gui)
      : camera(cam), sceneSource(_scene), gui(_gui) {}

    void Render(std::string) const;
  };
}