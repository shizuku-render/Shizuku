#pragma once

#include "GPU.hpp"
#include "Vector.hpp"

namespace Shizuku
{
  class Ray
  {
  public:
    Vec3<double> org, dir;
    Ray() = delete;
    Ray(const Ray& r) GPU : Ray(r.org, r.dir) {}
    Ray(const Vec3<double> _org, const Vec3<double> _dir) GPU : org(_org), dir(_dir) {}
  };
}
