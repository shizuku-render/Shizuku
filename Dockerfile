FROM rocm/rocm-terminal:2.0

ENV CMAKE_BASENAME cmake-3.13.2-Linux-x86_64
ENV PATH "/usr/local/${CMAKE_BASENAME}/bin:$PATH"

WORKDIR /usr/local
RUN sudo apt remove -y cmake \
  && cd /usr/local \
  && curl -sSL "https://github.com/Kitware/CMake/releases/download/v3.13.2/${CMAKE_BASENAME}.tar.gz" | sudo tar -zxv > /dev/null
