sudo apt remove -y cmake
cd /usr/local
curl -sSL "https://github.com/Kitware/CMake/releases/download/v3.13.2/${CMAKE_BASENAME}.tar.gz" | sudo tar -zxv
export PATH="/usr/local/${CMAKE_BASENAME}/bin:$PATH"
cd ${CI_PROJECT_DIR}
